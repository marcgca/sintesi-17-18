# Monitorización de temperatura y registro de movimientos con Raspberry Pi

Este proyecto ha sido realizado como trabajo final de grado medio de informática. 
Su función es la detección de movimiento con una webcam y monitorizar la temperatura detectada por los sensores en una Raspberry Pi.

---

Para realizar este proyecto son necesarios los siguientes elementos:

1. Raspberry Pi Model 3B
2. Webcam Logitech (para la detección de movimiento)
3. Sensor de temperatura Sunfounder con Breadboard
4. Bot de Telegram con Python3
5. Paquetes Motion y fswebcam para la detección de movimiento y captura de imágenes.

---

En la carpeta motion se encuentra la configuración de motion y la carpeta donde se guardan imágenes y vídeos.

En la carpeta python se encuentran todos los scripts necesarios para que el funcionamiento del proyecto sea correcto.

---

La creación y desarrollo completo se encuentra en la [Documentación del proyecto](https://gitlab.com/marcgc/sintesi-17-18/raw/master/Documentaci%C3%B3n_proyecto_final.pdf)