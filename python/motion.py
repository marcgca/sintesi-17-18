#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests, glob, os, json, time
import sys

## Accedemos al token mediante un fichero json
with open("/home/pi/python/token.json") as file:
        token = json.load(file)["token"]
url = 'https://api.telegram.org/bot'+token+'/'
## Accedemos al chat_id mediante un fichero json
with open("/home/pi/python/chat_id.json") as file:
        chat_id = json.load(file)["chat_id"]
## Indicamos dónde encontrar las imagenes
files = glob.glob('/home/pi/motion/motion-images/*.mp4') 
## Coge el último fichero creado en la variable files
last_file = max(files, key=os.path.getctime)

""" ## Detección de caras
path = "/home/pi/python/face"
os.chdir(path)
os.system('python3 detect.py')
 """
print("Movimiento")

## Manda un mensaje y una foto (la última) por cada ejecución del código.
r1 = requests.post(url+'sendMessage', data={"chat_id": chat_id, "text": 'MOVIMIENTO DETECTADO'})
File = last_file
r3 = requests.post(url+'sendVideo', files={'video': open(File, 'rb')}, data={"chat_id": chat_id}) 

