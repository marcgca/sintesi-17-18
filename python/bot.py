#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import glob, os, psutil, sys, subprocess, json, time, requests
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
import threading
from temp import MyTemp
print("---")
print("Iniciando Bot de Telegram de Monitorización")
print("by Marc G. & Matias C.")
print("---")

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',level=logging.INFO)

## Para que el token no esté en texto plano en el mismo fichero que usa el bot, el cual es fácil de acceder, utilizamos un json e "importamos" la variable.
with open("/home/pi/python/token.json") as file:
    token = json.load(file)["token"]
## Lo mismo, pero con el Chat_id.
with open("/home/pi/python/chat_id.json") as file:
    chat_id = json.load(file)["chat_id"]
updater = Updater(token)
dispatcher = updater.dispatcher
url = 'https://api.telegram.org/bot'+token+'/'

## Cuando se usa /start envía un mensaje diciendo la frase indicada. A su vez envía un mensaje por línea de comando para confirmar que ha funcionado.
def start(bot,update): 
    bot.send_message(chat_id=update.message.chat_id, text="Hola todo el mundo")
    print("Start")

## Cuando se usa /startmotion se activa la detección de movimiento y a su vez envía un mensaje confirmando la activación.
def startMotion(bot, update):
    res = subprocess.Popen(["motion", "-c /home/pi/motion/motion.conf"], shell=False, stdout=subprocess.PIPE, cwd="/home/pi/motion")
    bot.send_message(chat_id, text="Detección de movimiento: ACTIVADA")
    print("Startmotion")

## Cuando se usa /stopmotion se desactiva la detección de movimiento y a su vez envía un mensaje confirmando la desactivación.
def stopMotion(bot, update):
## Definimos qué va a buscar en la lista de procesos
    PROCNAME = "motion"
## La siguiente linea busca en la lista de procesos el PROCNAME indicado, y en el caso de encontrarlo, lo mata.
    for proc in psutil.process_iter():
        if proc.name() == PROCNAME:
            proc.kill()

    bot.send_message(chat_id, text="Detección de movimiento: DESACTIVADA")
    print("Stopmotion")

## Cuando se usa /status envía un mensaje diciendo el estado actual de la detección de movimiento [ACTIVO/INACTIVO]
def statusMotion(bot, update):
    ps = subprocess.check_output(('ps'))
    if (ps.find(b"motion")) == -1:
        bot.send_message(chat_id, text="Detección de movimiento: DESACTIVADA")
        print("STATUS: Desactivado")
    else:
        bot.send_message(chat_id, text="Detección de movimiento: ACTIVADA")
        print("STATUS: Activado")

## Cuando se usa /foto hace una foto instantanea de la webcam.
def captura(bot, update):
## Mensaje pre-foto
    bot.send_message(chat_id, text="Whiskey!!")
## Comando para hacer la foto + donde se guarda
    res = subprocess.Popen(["fswebcam", "-r 1280x720", "snap.jpg"], shell=False, stdout=subprocess.PIPE, cwd="/home/pi/motion/motion-images")
    res.wait()
## Mensaje con la foto
    bot.send_photo(chat_id, photo=open("/home/pi/motion/motion-images/snap.jpg", "rb"))
    print("Captura")

## Cuando se usa /clear limpia la carpeta donde se guardan las imágenes hechas por la captura de movimiento y las fotos individuales.
def clear(bot, update):
## Directorio de los videos
    directory="/home/pi/motion/motion-images"
## Nos movemos al directorio
    os.chdir(directory)
## Indicamos que 'files' son todas las imagenes .jpg
    files=glob.glob('*.*')
## Borramos cada imagen
    for filename in files:
        os.remove(filename)
    bot.send_message(chat_id, text="Todo limpio")
    print("Limpieza")

## Cuando se usa /last envía la última imagen hecha (ya sea por movimiento o manual)
def ultima(bot, update):
    files = glob.glob('/home/pi/motion/motion-images/*.jpg')
## Intenta enviar la última imagen
    try:
        lastfile = max(files, key=os.path.getctime)
        file_handler = open(lastfile, 'rb')
        bot.send_photo(chat_id, photo=file_handler)
        print("Última foto enviada")
## Si resulta que no hay ninguna imagen, envía el siguiente mensaje
    except(ValueError):
        bot.send_message(chat_id, text="Lo siento, todas las fotos han sido borradas")
        print("Última foto NO enviada")

## Cuando se usa /temp envía la temperatura actual
def temperatura(bot,update):
## Llamamos a la función que lee la temperatura
    temp = round(MyTemp.loop())
    bot.send_message(chat_id, text="La temperatura actual es:"+str(temp)+"ºC")
    print("Temperatura: "+str(temp))

# Comandos
## Todos los handler de todas las funciones del bot. Estas se las llama con / y el nombre.
#   Inicio
dispatcher.add_handler(CommandHandler('Start',start))
#   Motion
#       /startmotion
startMotion_handler = CommandHandler('startmotion', startMotion)
dispatcher.add_handler(startMotion_handler)
#       /stopmotion
stopMotion_handler = CommandHandler('stopmotion', stopMotion)
dispatcher.add_handler(stopMotion_handler)
#       /status
statusMotion_handler = CommandHandler('status', statusMotion)
dispatcher.add_handler(statusMotion_handler)
#       /foto
captura_handler = CommandHandler('foto', captura)
dispatcher.add_handler(captura_handler)
#       /clear
clear_handler = CommandHandler('clear', clear)
dispatcher.add_handler(clear_handler)
#       /last
ultima_handler = CommandHandler('last', ultima)
dispatcher.add_handler(ultima_handler)
#       /temp
temp_handler = CommandHandler('temp', temperatura)
dispatcher.add_handler(temp_handler)

if __name__ == '__main__':
    updater.start_polling()
    updater.idle()